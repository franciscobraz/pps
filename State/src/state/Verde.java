package estados;

public class Verde extends Estado {

	@Override
	public Estado tick() {
		return new Amarelo();
	}

	@Override
	public Estado panic() {
		return new Vermelho();
	}

	@Override
	public Estado on() {
		return new Tick();
	}

	@Override
	public Estado off() {
		return new Off();
	}
	
	@Override
	public String status() {
		return "Verde";
	}

}
