package estados;

public class Vermelho extends Estado {

	@Override
	public Estado tick() {
		return new Verde();
	}

	@Override
	public Estado panic() {
		return new Vermelho();
	}

	@Override
	public Estado on() {
		return new Tick();
	}

	@Override
	public Estado off() {
		return new Off();
	}
	
	@Override
	public String status() {
		return "Vermelho";
	}

}
