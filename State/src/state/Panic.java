package estados;

public class Panic extends Estado {

	@Override
	public Estado tick() {
		return new Tick();
	}

	@Override
	public Estado panic() {
		return new Vermelho();
	}

	@Override
	public Estado on() {
		return new Tick();
	}

	@Override
	public Estado off() {
		return new Off();
	
	}

	@Override
	public String status() {
		return "Vermelho Intermitente";
	}

}
