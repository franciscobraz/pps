import java.util.ArrayList;
import java.util.Collection;

public class WeatherData {

	private Collection <WeatherDataListener> weatherDataListeners = new ArrayList<WeatherDataListener>();
	
	public synchronized void addWeatherData(WeatherDataListener w) {
		if (!weatherDataListeners.contains(w)) {
			weatherDataListeners.add(w);
		}
	}
	
	public synchronized void removeWeatherData(WeatherDataListener w) {
		weatherDataListeners.remove(w);
	}
	
	public void mudouTemperatura() {
		disparaTemperatura();
	}
	
	public void mudouHumidade() {
		disparaHumidade();
	}
	
	public void mudouPressao() {
		disparaPressao();
	}
	
	
	
	
	/*
	 * ACOES
	 */
	
	private void disparaTemperatura() {
		Collection <WeatherDataListener> wd;		
		synchronized (this) {
			wd = (Collection) (((ArrayList) weatherDataListeners).clone());
		}
		WeatherDataEvent evento = new WeatherDataEvent(this);
		for(WeatherDataListener d : wd) {
			d.pegarTemperatura(evento);
		}
	}
	
	private void disparaHumidade() {
		Collection <WeatherDataListener> wd;
		synchronized (this) {
			wd = (Collection) (((ArrayList) weatherDataListeners).clone());
		}
		WeatherDataEvent evento = new WeatherDataEvent(this);
		for(WeatherDataListener d : wd) {
			d.pegarHumidade(evento);
		}
	}
	
	private void disparaPressao() {
		Collection <WeatherDataListener> wd;		
		synchronized (this) {
			wd = (Collection) (((ArrayList) weatherDataListeners).clone());
		}		
		WeatherDataEvent evento = new WeatherDataEvent(this);
		for(WeatherDataListener d : wd) {
			d.pegarPressao(evento);
		}
	}
}
 