public class Exemplo {

	public static void main(String[] args) {
		
		WeatherData wd = new WeatherData();
		
		//adiciona dispositivos
		Telejornal telejornal = new Telejornal();
		Pesquisador pesquisador = new Pesquisador();
		Dispositivo dispositivo = new Dispositivo();
		
		
		wd.addWeatherData(dispositivo);
		wd.addWeatherData(pesquisador);
		wd.addWeatherData(telejornal);
		
		wd.mudouPressao();
		wd.mudouHumidade();
		
	}

}
