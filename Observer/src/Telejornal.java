
public class Telejornal implements WeatherDataListener {

	@Override
	public void pegarTemperatura(WeatherDataEvent e) {
		System.out.println("Telejornal recebeu a mudanca de temperatura");
	}

	@Override
	public void pegarHumidade(WeatherDataEvent e) {
		System.out.println("Telejornal recebeu a mudanca de Humidade");
	}

	@Override
	public void pegarPressao(WeatherDataEvent e) {
		System.out.println("Telejornal recebeu a mudanca de Pressao");
	}
	
}
