
public class Bradesco extends Banco {

	public Bradesco(Mediator m) {
		super(m);
	}

    @Override
    public void receberTransferencia(double valor) {
        System.out.println("Conta Bradesco recebeu: " + valor);
    }
}
