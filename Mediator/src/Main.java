
public class Main {
	public static void main(String[] args) {
	    TransferenciaMediator mediador = new TransferenciaMediator();
	 
	    BB bb = new BB(mediador);
	    CEF cef = new CEF(mediador);
	    Itau itau = new Itau(mediador);
	    Bradesco bradesco = new Bradesco(mediador);
	 
	    mediador.adicionarBanco(bb);
	    mediador.adicionarBanco(cef);
	    mediador.adicionarBanco(itau);
	    mediador.adicionarBanco(bradesco);
	     
	    bradesco.mediator.enviar(500.00, bb);
	    cef.mediator.enviar(1500.00, itau);
	    itau.mediator.enviar(250.00, bradesco);
	    bb.mediator.enviar(720.00, cef);
	}
}
