
public class CEF extends Banco {

	public CEF(Mediator m) {
		super(m);
	}

	@Override
	public void receberTransferencia(double valor) {
		System.out.println("Conta CEF recebeu: " + valor);
	}

}
