import java.util.ArrayList;


public class TransferenciaMediator implements Mediator {

	protected ArrayList<Banco> bancos;

	public TransferenciaMediator() {
		bancos = new ArrayList<Banco>();
	}

	public void adicionarBanco(Banco banco) {
		bancos.add(banco);
	}

	@Override
	public void enviar(double valor, Banco bank) {
		for (Banco banco : bancos) {
			if (banco == bank) {
				banco.receberTransferencia(valor);
			}
		}
	}

}
