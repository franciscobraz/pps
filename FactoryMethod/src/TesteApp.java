

public class TesteApp {

	public static void main(String args[]) {
		FactoryNomeSobrenome factory = new FactoryNomeSobrenome();
		String[] nomes = { "Francisc, Braz", "Fernanda Melo", "Petronio, Medeiros" };

		for (String nome : nomes) {			
			System.out.println(factory.criar(nome).getNome());
		}

	}
}