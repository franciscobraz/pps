package pdp;

public class Quadrado extends FiguraComposta implements FiguraGenerica {
	
	public void addLado(Reta r){
		if(this.retas.size() < 1){
			this.addParte(r);
		}else{
			System.out.println("Quadrado ja esta completo!");
		}
	}
	
	@Override
	public void draw() {
		System.out.println("Quadrado");
	}

	@Override
	public void area() {
		double area;
		double lado;
		
		lado = this.retas.get(0).comprimento;
		area = Math.pow(lado, 2);
		System.out.println("Area: "+area);
	}

	@Override
	public void volume() {
		System.out.println("Volume");
	}

}
