package pdp;

public class Manipulacao {

	public static void main(String[] args) {
		Reta r1, r2, r3, r4;
		r1 = new Reta(5.0);
		r2 = new Reta(4.0);
		r3 = new Reta(6.0);
		r4 = new Reta(8.0);
		
		Triangulo triangulo = new Triangulo();
		triangulo.addLado(r1);
		triangulo.addLado(r4);
		triangulo.addLado(r3);
		triangulo.areaFigura();
		triangulo.perimetroFigura();

		System.out.println();
		
		Quadrilatero quadrilatero = new Quadrilatero();
		quadrilatero.addLado(r2);
		quadrilatero.addLado(r3);
		quadrilatero.addLado(r1);
		quadrilatero.addLado(r4);
		quadrilatero.areaFigura();
		quadrilatero.perimetroFigura();
		
		System.out.println();
		
		Circulo circulo = new Circulo();
		circulo.addLado(r2);
		circulo.areaFigura();
		circulo.perimetroFigura();
		
		System.out.println();
		
		Quadrado quadrado = new Quadrado();
		quadrado.addLado(r1);
		quadrado.draw();
		quadrado.area();
		quadrado.volume();
		
		System.out.println();
		
		Losango losango = new Losango();
		losango.addLado(r1);
		losango.draw();
		losango.area();
		losango.volume();
	}

}
